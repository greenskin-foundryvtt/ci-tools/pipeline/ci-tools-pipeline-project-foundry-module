# ci-tools-pipeline-project-foundry-module

This module contains complete gitlab ci pipeline for foundry module project

## pipeline-foundry-module.yml

Since 1.0.0

Complete pipeline for a standard foundry module project

- build and package foundry module
- complete release jobs with assets publish from version

### Dependencies

- release-common
- release-node

### Usage

Exemple for a foundry module

```
variables:
  MODULE_DIRS: lang styles
  MODULE_ZIP_DIRS: elements lang styles

include:
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release'
    ref: 1.0.0
    file: '/release-common.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release-node'
    ref: 1.0.1
    file: '/release-node.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-project-foundry-module'
    ref: 1.0.0
    file: '/pipeline-foundry-module.yml'


```
